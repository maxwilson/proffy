import React from 'react';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';
import zoaImg from '../../assets/teste.jpg';

import './styles.css';

function TeacherItem() {
  return (
    <article className="teacher-item">
      <header>
        <img src={zoaImg} alt="perfil"/>
        <div>
          <strong>Davi </strong>
          <span>Sexologia</span>
        </div>
      </header>
      <p>
        Entusiasta das melhores tecnicas de sexo avançada.
        <br /><br />
        Apaixonado por dar cu nas melhores posições. Seu dilema é sento até quebrar sua rola.
      </p>

      <footer>
        <p>
          Preço/hora 
          <strong>R$ 2,00</strong>
        </p>
        <button type="button">
          <img src={whatsappIcon} alt="Whatsapp"/>
          Entrar em contato
        </button>
      </footer>
    </article>
  );
}

export default TeacherItem;

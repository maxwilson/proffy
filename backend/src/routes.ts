import express from 'express';
import ClassesController from './controllers/ClassesController';
import ConnectionsController from './controllers/ConnectionsController';

const routes = express.Router();

const ClassesControllers = new ClassesController();
const ConnectionsControllers = new ConnectionsController();

//classes
routes.post('/classes', ClassesControllers.create);
routes.get('/classes', ClassesControllers.index);

//connections
routes.post('/connections', ConnectionsControllers.create);

export default routes;